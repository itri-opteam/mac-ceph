```sh
#!/bin/bash

# 刪除 secret
kubectl delete secret ceph-secret-admin --namespace kube-system
# 透過 kubectl 取得 pv ，然後比對 ceph 的字眼，之把印出的 ceph pv 透過 kubectl 刪除
kubectl get pv | awk '/ceph/ {print $1}' | xargs -I{} kubectl delete pv {}

# 刪除相關的 ceph objects 
kubectl delete storageclass rbd --all
kubectl delete storageclass cephfs --all
kubectl delete namespace ceph
kubectl label nodes --all node-type-
kubectl delete -f ceph-rbac.yaml
```
