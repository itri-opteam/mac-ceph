# Create CEPH Cluster
## 檔案位置
- 共用硬碟的位置: `\\fs.ccma.itri.org.tw\H_Group\Mac 交接\matchbox\ceph\create_ceph_cluster.sh`
- [create_ceph_cluster.sh](create_ceph_cluster.sh)

## 程式執行
```sh
$ sh create_ceph_cluster.sh
```

## 程式說明
```sh
#!/bin/bash

# 設置 debug shell scripts(-x) ，且遇到錯誤直接停止(-e) 的環境變數
set -ex
# 改變檔案權限，將 sh 檔案都設為可執行模式
chmod +x *.sh 
# 定義變數
ceph_ver="luminous"
ceph_tag="tag-stable-3.0-$ceph_ver-ubuntu-16.04"

# 來等個 15 秒，讓你有後悔裝 ceph 的權利(15 秒內你可以取消安裝 ceph ，要不然就繼續執行囉！)
timeout=15
echo -e "Will deloy CEPH $ceph_ver to K8S....\nPress any key to cancel the deploymnt."
if read -n 1 -t $timeout;then
   echo "Abort deployment......"
   exit
fi

# 使用 pip 安裝 jinja2-cli
#install requirements
pip install jinja2-cli
# 下載 sigil 並解壓縮後將該執行檔放到 /usr/local/bin 底下
curl -L "https://github.com/gliderlabs/sigil/releases/download/v0.4.0/sigil_0.4.0_$(uname -sm|tr \  _).tgz"   | tar -zxC /usr/local/bin

# 透過 k8s 建立 ceph-rbac 的 ClusterRole 等
#apply rbac
kubectl create -f ceph-rbac.yaml


sed -i "s/ceph\/daemon.*$/ceph\/daemon:$ceph_tag/g" *.yaml 

# 設環境變數 (不過 pod_cidr 在？)
export osd_cluster_network=`cat pod_cidr`
export osd_public_network=$osd_cluster_network

# 執行 create_secrets.sh 這支程式。
./create_secrets.sh "$@"

# 透過 ceph-*.yaml 來建立各種 objects
kubectl create \
-f ceph-mds-v1-dp.yaml \
-f ceph-mon-v1-svc.yaml \
-f ceph-mon-v1-dp.yaml \
-f ceph-mon-check-v1-dp.yaml \
-f ceph-osd-v1-ds.yaml \
-f ceph-mgr-v1-dp.yaml \
--namespace=ceph

# 印出以下資訊
# 請加標籤到你的 storage nodes 和測試 cephfs/rbd
# 1. 加標籤 (kubectl label node <nodename> node-type=storage)
# 2. 確認 ceph 的健康狀態 (sh ceph-health-check.sh)
# 3. 測試 cephfs 和 rbd (sh ceph-test.sh)
echo "Please label your storage nodes and test cephfs/rbd."
echo "(1) Label storage nodes"
echo "kubectl label node <nodename> node-type=storage"
echo "(2) Check ceph health"
echo "sh ceph-health-check.sh"
echo "(3) Test cephfs and rbd"
echo "sh ceph-test.sh"
```




Ref: 
Different from https://github.com/ceph/ceph-container/blob/2b88edbd8ffb6727a0d8b5edeac0e4cb43dfe65a/examples/kubernetes/create_ceph_cluster.sh
