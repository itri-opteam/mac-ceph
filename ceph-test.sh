#!/bin/bash
set -ex
function waiting()
{
    while kubectl get pod -n ceph | grep 'ContainerCreating\|CrashLoopBackOff\|Completed' #2>&1 > /dev/null
    do
        echo $status
        sleep 2
    done
}
echo "cephfs testing...."
kubectl create -f test/ceph-cephfs-test.yaml
waiting
kubectl exec -it --namespace=ceph ceph-cephfs-test df
echo "cephfs done...."

echo "ceph rbd testing...."
export MON_POD_NAME=`kubectl get pods --selector="app=ceph,daemon=mon" --output=template --template="{{with index .items 0}}{{.metadata.name}}{{end}}" --namespace=ceph`
kubectl exec ${MON_POD_NAME}  -n ceph -- ceph osd pool create rbd 256
kubectl exec ${MON_POD_NAME}  -n ceph -- rbd pool init rbd
kubectl exec -it $MON_POD_NAME --namespace=ceph -- rbd create ceph-rbd-test --size 1G
kubectl exec -it $MON_POD_NAME --namespace=ceph -- rbd info ceph-rbd-test
kubectl create -f test/ceph-rbd-test.yaml
waiting
kubectl exec -it --namespace=ceph ceph-rbd-test -- df -h

timeout=60
echo -e "Please check result in $timeout secs\nPress any key to cancel cleaning."
if read -n 1 -t $timeout;then
   echo "Abort cleaning......"
   exit
fi
echo "delete pods"
kubectl delete -f test/
echo "delete rbd"
kubectl exec ${MON_POD_NAME}  -n ceph -- ceph osd pool delete rbd rbd --yes-i-really-really-mean-it


