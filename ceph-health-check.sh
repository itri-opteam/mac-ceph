#!/bin/bash
set -ex
export MON_POD_NAME=`kubectl get pods --selector="app=ceph,daemon=mon" --output=template --template="{{with index .items 0}}{{.metadata.name}}{{end}}" --namespace=ceph`
kubectl exec -it $MON_POD_NAME -n ceph ceph status