#!/bin/bash
set -ex
cd deploy
NAMESPACE=ceph # change this if you want to deploy it in another namespace
# if [ ! "$1" ];then
#     echo "Using default namespace: ceph"
# else
#     echo "Using namespce:$1"
#     NAMESPACE=$1
# fi
# timeout=15
# echo -e "Please check config in $timeout secs\nPress any key to cancel deploymnt."
# if read -n 1 -t $timeout;then
#    echo "About deployment......"
#    exit
# fi

# sed -r -i "s/namespace: [^ ]+/namespace: $NAMESPACE/g" ./rbac/clusterrolebinding.yaml
# kubectl -n $NAMESPACE apply -f ./rbac

kubectl -n $NAMESPACE apply -f deploy/rbac/deployment.yaml
kubectl -n $NAMESPACE apply -f deploy/class.yaml