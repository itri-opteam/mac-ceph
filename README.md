# 中文說明
## Install
### 0. prepare
1. 
```sh
$ apt-get install ceph-fs-common ceph-common
```
2. 一定要用 flannel 網路
3. After setup k8s cluster, let's exec this command on master1 node
```sh
$ kubectl taint nodes --all node-role.kubernetes.io/master-
```

### 1. 在所有的 host 上修改 `/etc/resolv.conf`
第一個 nameserver 是 K8s 的 dns IP
可以透過下面的方式查詢:
```sh
$ kubectl get service -n kube-system -o wide
kube-dns                ClusterIP   10.96.0.10       <none>        53/UDP,53/TCP       13d       k8s-app=kube-dns
```

- `vim /etc/resolv.conf`

```sh
search default.svc.cluster.local svc.cluster.local cluster.local
options ndots:5
options timeout:1
nameserver 10.96.0.10
nameserver 8.8.8.8
nameserver 8.8.4.4
```
### 2. 修改 `generator/ceph.conf` 的欄位
```sh
cluster_network = 10.244.0.0/16
public_network = 10.244.0.0/16
```
### 3. 修改 `pod_cidr`
```sh
10.244.0.0/16
```

### 4. Create CEPH Cluster
```sh
$ sh create_ceph_cluster.sh && kubectl label nodes node-type=storage --all
```

### 5. View All
```sh
$ kubectl get all --namespace=ceph -o wide
```
### Troubleshooting
if you meet this situation
```sh
po/ceph-mgr-d59b767f6-bcrrp          0/1       Running   0          11m       10.223.5.204   momo4
```
Please delete this pod and let see.
```sh
$ kubectl logs po/ceph-mgr-d59b767f6-bcrrp -n ceph
nameserver 10.96.0.10
nameserver 8.8.8.8
nameserver 8.8.4.4
search ceph.svc.cluster.local svc.cluster.local cluster.local
2018-03-30 05:56:49  /entrypoint.sh: k8s: config is stored as k8s secrets.
2018-03-30 05:56:49  /entrypoint.sh: k8s: does not generate the admin key. Use Kubernetes secrets instead.
2018-03-30 05:56:49  /entrypoint.sh: SUCCESS
ceph version 12.2.3 (2dab17a455c09584f2a85e6b10888337d1ec8949) luminous (stable)
2018-03-30 05:56:53  /entrypoint.sh: SUCCESS
exec: PID 186: spawning /usr/bin/ceph-mgr --cluster ceph --setuser ceph --setgroup ceph -d -i momo4
2018-03-30 05:56:53.719359 7fa84b780680  0 set uid:gid to 64045:64045 (ceph:ceph)
2018-03-30 05:56:53.719391 7fa84b780680  0 ceph version 12.2.3 (2dab17a455c09584f2a85e6b10888337d1ec8949) luminous (stable), process (unknown), pid 186
2018-03-30 05:56:53.720262 7fa84b780680  0 pidfile_write: ignore empty --pid-file
2018-03-30 05:56:53.733195 7fa84b780680  1 mgr send_beacon standby
2018-03-30 05:56:54.717261 7fa842ab0700  1 mgr handle_mgr_map Activating!
2018-03-30 05:56:54.717783 7fa842ab0700  1 mgr handle_mgr_map I am now activating
2018-03-30 05:56:54.765764 7fa83e8a1700  1 mgr send_beacon active
2018-03-30 05:56:55.734140 7fa83f8a3700  1 mgr send_beacon active
2018-03-30 05:56:55.735192 7fa83f8a3700  1 mgr.server send_report Not sending PG status to monitor yet, waiting for OSDs
2018-03-30 05:56:57.735335 7fa83f8a3700  1 mgr send_beacon active
2018-03-30 05:56:57.736039 7fa83f8a3700  1 mgr.server send_report Not sending PG status to monitor yet, waiting for OSDs
2018-03-30 05:56:59.736177 7fa83f8a3700  1 mgr send_beacon active
2018-03-30 05:56:59.736862 7fa83f8a3700  1 mgr.server send_report Not sending PG status to monitor yet, waiting for OSDs
2018-03-30 05:57:01.736981 7fa83f8a3700  1 mgr send_beacon active
2018-03-30 05:57:01.737795 7fa83f8a3700  1 mgr.server send_report Not sending PG status to monitor yet, waiting for OSDs
2018-03-30 05:57:03.737905 7fa83f8a3700  1 mgr send_beacon active
2018-03-30 05:57:03.738757 7fa83f8a3700  1 mgr.server send_report Not sending PG status to monitor yet, waiting for OSDs
2018-03-30 05:57:05.738870 7fa83f8a3700  1 mgr send_beacon active
2018-03-30 05:57:05.739671 7fa83f8a3700  1 mgr.server send_report Not sending PG status to monitor yet, waiting for OSDs
2018-03-30 05:57:07.739800 7fa83f8a3700  1 mgr send_beacon active
2018-03-30 05:57:07.740348 7fa83f8a3700  1 mgr.server send_report Not sending PG status to monitor yet, waiting for OSDs
2018-03-30 05:57:09.740450 7fa83f8a3700  1 mgr send_beacon active
2018-03-30 05:57:09.741253 7fa83f8a3700  1 mgr.server send_report Not sending PG status to monitor yet, waiting for OSDs
2018-03-30 05:57:11.741378 7fa83f8a3700  1 mgr send_beacon active
2018-03-30 05:57:11.742102 7fa83f8a3700  1 mgr.server send_report Not sending PG status to monitor yet, waiting for OSDs
2018-03-30 05:57:13.742207 7fa83f8a3700  1 mgr send_beacon active
2018-03-30 05:57:13.743032 7fa83f8a3700  1 mgr.server send_report Not sending PG status to monitor yet, waiting for OSDs
2018-03-30 05:57:15.743143 7fa83f8a3700  1 mgr send_beacon active
2018-03-30 05:57:15.743839 7fa83f8a3700  1 mgr.server send_report Giving up on OSDs that haven't reported yet, sending potentially incomplete PG state to mon
2018-03-30 05:57:15.743995 7fa83f8a3700  0 Cannot get stat of OSD 3
2018-03-30 05:57:17.744333 7fa83f8a3700  1 mgr send_beacon active
2018-03-30 05:57:17.745187 7fa83f8a3700  0 Cannot get stat of OSD 3
2018-03-30 05:57:19.745589 7fa83f8a3700  1 mgr send_beacon active
2018-03-30 05:57:19.746358 7fa83f8a3700  0 Cannot get stat of OSD 3
2018-03-30 05:57:21.746774 7fa83f8a3700  1 mgr send_beacon active
2018-03-30 05:57:21.747316 7fa83f8a3700  0 Cannot get stat of OSD 3
2018-03-30 05:57:23.747558 7fa83f8a3700  1 mgr send_beacon active
2018-03-30 05:57:23.748181 7fa83f8a3700  0 Cannot get stat of OSD 3
2018-03-30 05:57:25.748511 7fa83f8a3700  1 mgr send_beacon active
```

### RDB provisioner
```sh
$ cd rbd-provisioner
$ sh deploy-rbd.sh
```

### Delete
```sh
kubectl delete secret ceph-secret-admin --namespace kube-system
kubectl get pv | awk '/ceph/ {print $1}' | xargs -I{} kubectl delete pv {}
kubectl delete storageclass rbd --all
kubectl delete storageclass cephfs --all
kubectl delete namespace ceph
kubectl label nodes --all node-type-
kubectl delete -f ceph-rbac.yaml
```

---

### Create CEPH Cluster
- 共用硬碟: `\\fs.ccma.itri.org.tw\H_Group\Mac 交接\matchbox\ceph\create_ceph_cluster.sh`
- [Detail](docs/create_ceph.md)
```sh
$ sh create_ceph_cluster.sh
```
(Different from https://github.com/ceph/ceph-container/blob/2b88edbd8ffb6727a0d8b5edeac0e4cb43dfe65a/examples/kubernetes/create_ceph_cluster.sh)

### Delete CEPH Cluster
- 共用硬碟: `\\fs.ccma.itri.org.tw\H_Group\Mac 交接\matchbox\ceph\delete_ceph_cluster.sh`
- [Detail](docs/delete_ceph.md)
```sh
$ sh delete_ceph_cluster.sh
```
(Different from https://github.com/ceph/ceph-container/blob/2b88edbd8ffb6727a0d8b5edeac0e4cb43dfe65a/examples/kubernetes/delete_ceph_cluster.sh)

### RDB provisioner
#### Create RDB provisioner
- 共用硬碟: `\\fs.ccma.itri.org.tw\H_Group\Mac 交接\matchbox\ceph\rbd-provisioner`
- [Detail](docs/deploy_rbd.md)
```sh
$ cd rbd-provisioner
$ sh deploy-rbd.sh
```

#### Apply example
```sh
$ kubectl -f examples
```

Ref:
- https://github.com/kubernetes-incubator/external-storage
- https://hub.docker.com/r/ceph/daemon/tags/
    - Current tag: tag-stable-3.0-luminous-ubuntu-16.04
- https://github.com/ceph/ceph-container


