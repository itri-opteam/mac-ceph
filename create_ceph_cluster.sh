#!/bin/bash
set -ex
chmod +x *.sh 
ceph_ver="luminous"

timeout=15
echo -e "Will deloy CEPH $ceph_ver to K8S....\nPress any key to cancel the deploymnt."
if read -n 1 -t $timeout;then
   echo "Abort deployment......"
   exit
fi

#install requirements
pip install jinja2-cli
curl -L "https://github.com/gliderlabs/sigil/releases/download/v0.4.0/sigil_0.4.0_$(uname -sm|tr \  _).tgz"   | tar -zxC /usr/local/bin

#apply rbac
kubectl create -f ceph-rbac.yaml

sed -i "s/ceph\/daemon:latest/ceph\/daemon:tag-build-master-$ceph_ver-ubuntu-16.04/g" ceph-mds*.yaml 

export osd_cluster_network=`cat pod_cidr`
export osd_public_network=$osd_cluster_network

export cluster_network=`cat pod_cidr`
export public_network=$osd_cluster_network

./create_secrets.sh "$@"

kubectl create \
-f ceph-mds-v1-dp.yaml \
-f ceph-mon-v1-svc.yaml \
-f ceph-mon-v1-dp.yaml \
-f ceph-mon-check-v1-dp.yaml \
-f ceph-osd-v1-ds.yaml \
-f ceph-mgr-v1-dp.yaml \
--namespace=ceph

echo "Please label your storage nodes and test cephfs/rbd."
echo "(1) Label storage nodes"
echo "kubectl label node <nodename> node-type=storage"
echo "(2) Check ceph health"
echo "sh ceph-health-check.sh"
echo "(3) Test cephfs and rbd"
echo "sh ceph-test.sh"
