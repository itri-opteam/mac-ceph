#!/bin/sh

kubectl delete secret ceph-secret-admin --namespace kube-system
kubectl get pv | awk '/ceph/ {print $1}' | xargs -I{} kubectl delete pv {}
kubectl delete storageclass rbd --all
kubectl delete storageclass cephfs --all
kubectl delete namespace ceph
kubectl label nodes --all node-type-
kubectl delete -f ceph-rbac.yaml
